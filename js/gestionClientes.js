var clientesObtenidos;

function getClientes(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONClientes.value[i].Country;

    var columnaBandera = document.createElement("td");
    var imagen = document.createElement("img");
    imagen.classList.add("flag");
    var pais = JSONClientes.value[i].Country;
    if (pais == "UK") {
      pais = "United-Kingdom";
    }
    imagen.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + pais +".png";
    columnaBandera.appendChild(imagen);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
